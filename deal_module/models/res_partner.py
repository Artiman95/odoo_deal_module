from odoo import models, fields, api


class ResPartner(models.Model):
    _inherit = 'res.partner'

    contract = fields.Integer(string="Contracts", compute='_compute_contract_count')
    contact_person_ids = fields.One2many(comodel_name='deal.docs', inverse_name='contact_person')

    @api.depends('contact_person_ids')
    def _compute_contract_count(self):
        for partner in self:
            partner.contract = len(partner.contact_person_ids)

    def action_contracts_counter(self):
        return {
            'type': 'ir.actions.act_window',
            'name': 'Contracts',
            'view_mode': 'tree,form',
            'res_model': 'deal.docs',
            'domain': [('contact_person', '=', self.id)]
        }

