from odoo import models, fields, api, _
from odoo.exceptions import UserError


class SaleOrder(models.Model):
    _inherit = "sale.order"

    def action_confirm(self):
        contract_ids = self.partner_id.contact_person_ids
        for contract in contract_ids:
            if contract.state == "in sign":
                contract_id = contract
                break
            raise UserError("No contract on sign")

        order_amount = sum(self.order_line.mapped("price_total"))
        transaction_amount = contract_id.amount_total

        sale_order_ids = self.partner_id.sale_order_ids
        contract_start_date = contract_id.date_start
        contract_end_date = contract_id.date_end
        sale_orders = []
        if not contract_end_date >= self.date_order.date() and contract_start_date <= self.date_order.date():
            raise UserError("No contract on sign at this date")
        if sale_order_ids:
            for sale_order_id in sale_order_ids:
                if contract_end_date >= sale_order_id.date_order.date() and contract_start_date <= sale_order_id.date_order.date() and sale_order_id.state in ["sent", "sale"]:
                    sale_orders.append(sale_order_id)

        total_order_amount = 0
        for sale_order_id in sale_orders:
            total_order_amount = total_order_amount + sum(sale_order_id.order_line.mapped("price_total"))

        total_order_amount = total_order_amount + order_amount

        if total_order_amount > transaction_amount:
            raise UserError("Insufficient funds")

        if self._get_forbidden_state_confirm() & set(self.mapped('state')):
            raise UserError(_(
                'It is not allowed to confirm an order in the following states: %s'
            ) % (', '.join(self._get_forbidden_state_confirm())))

        for order in self.filtered(lambda order: order.partner_id not in order.message_partner_ids):
            order.message_subscribe([order.partner_id.id])
        self.write(self._prepare_confirmation_values())

        context = self._context.copy()
        context.pop('default_name', None)

        self.with_context(context)._action_confirm()
        if self.env.user.has_group('sale.group_auto_done_setting'):
            self.action_done()
        return True
