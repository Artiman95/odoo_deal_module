from odoo import models, fields, api
from odoo.exceptions import UserError
from datetime import date


class DealDocs(models.Model):
    _name = "deal.docs"
    _description = "deal document"
    _inherit = ["mail.thread", "mail.activity.mixin"]

    def get_user_id(self):
        user_id = self.env.user.id
        return user_id

    name = fields.Char(string="Name")
    doc_author = fields.Many2one(comodel_name="res.users", string="User", default=lambda self: self.get_user_id(), readonly=True)
    state = fields.Selection([("draft", "Draft"), ("in progress", "In Progress"), ("in sign", "In Sign"), ("archive", "Archive")], string="Status", default="draft")
    contact_person = fields.Many2one(comodel_name="res.partner", string="Contact person", domain="[('is_company', '=', False)]")
    contact_phone = fields.Char(string="Contact phone")
    date_start = fields.Date(string="Date start")
    date_end = fields.Date(string="Date end")
    send_mail = fields.Boolean(string="Sending by mail")
    message_ids = fields.One2many("mail.message", "res_id", string="Messages")
    currency_id = fields.Many2one(comodel_name="res.currency", string="currency", default=lambda self: self.env.company.currency_id, readonly=True)
    amount_total = fields.Monetary(string="Total amount", currency_field="currency_id")
    total_order_amount = fields.Monetary(string="Total order amount", currency_field="currency_id", compute="_compute_total_order_amount")

    def _compute_total_order_amount(self):
        for record in self:
            summ_amount = 0
            for sale_order_id in record.contact_person.sale_order_ids:
                if record.date_end >= sale_order_id.date_order.date() and record.date_start <= sale_order_id.date_order.date() and sale_order_id.state in ["sent", "sale"]:
                    summ_amount = summ_amount + sum(sale_order_id.order_line.mapped("price_total"))
            record.total_order_amount = summ_amount

    def button_take_to_work(self):
        if not self.contact_person:
            raise UserError('Contact person must be specified')
        self.state = "in progress"

    def button_in_sign(self):
        if not self.contact_phone:
            raise UserError("Chose contact with phone")
        if not self.date_start:
            raise UserError("Add start day of contract")
        if not self.date_end:
            raise UserError("Add end day of contract")
        if self.date_end < self.date_start:
            raise UserError("End date cannot be earlier than start date.")
        if self.date_end < date.today():
            raise UserError("Contract with this date can not be add")

        all_contracts = self.contact_person.contact_person_ids
        filtered_contracts_ids = []

        for contract in all_contracts:
            if contract.state == "in sign":
                filtered_contracts_ids.append(contract)

        for filtered_contracts_id in filtered_contracts_ids:
            if filtered_contracts_id.date_start <= self.date_end and filtered_contracts_id.date_end >= self.date_start:
                raise UserError("The contact already has a contract within the specified time period")

        self.state = "in sign"

    def button_archive(self):
        message = f"Contract {self.name} delivered in archive {self.doc_author.name}"
        self.message_post(body=message)
        self.state = "archive"

    def exit_button(self):
        return {
            'type': 'ir.actions.act_window',
            'name': 'Contracts',
            'view_mode': 'tree,form',
            'res_model': 'deal.docs',
        }

    @api.onchange('contact_person')
    def get_contact_phone(self):
        self.contact_phone = self.contact_person.phone
