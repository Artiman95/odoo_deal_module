# -*- coding: utf-8 -*-
{
    'name': 'Deal module',
    'version': '14.0.1.0',
    'summary': 'Deal Module Software',
    'sequence': -90,
    'description': """Deal Module Software""",
    'category': 'Productivity',
    'website': 'https://www.google.com',
    'depends': ["contacts", "mail", "sale_management"],
    'data': [
        'security/ir.model.access.csv',
        'views/deal_view.xml',
        'views/menu_view.xml',
        'views/res_partner_contract_view.xml'
    ],
    'demo': [],
    'qweb': [],
    'installable': True,
    'application': True,
    'auto_install': False,
}
